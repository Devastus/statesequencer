﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace StateMachine
{
    public class StateSequencerWindow : EditorWindow
    {

        private static StateSequencerWindow window;
        private Vector2 minimumSize = new Vector2(300, 300);
        private StateSequencer_WorkView view;
        private StateSequencer_BlackboardView blackboard;
        private StateSequencer seq;
        private float viewPercentage = 0.75f;
        private GUISkin guiSkin;

        public static void ShowWindow(StateSequencer s)
        {
            window = EditorWindow.GetWindow<StateSequencerWindow>();
            window.seq = s;
            window.titleContent = new GUIContent("State Sequencer");
            window.minSize = window.minimumSize;
        }

        void OnGUI()
        {
            if (view == null || blackboard == null)
            {
                CreateView();
            }
            else
            {
                Event e = Event.current;
                view.UpdateView(position, new Rect(0, 0, viewPercentage, 1), e, seq, guiSkin);
                blackboard.UpdateView(position, new Rect(viewPercentage, 0, 1-viewPercentage, 1), e, seq, guiSkin);
                Repaint();
            }
        }

        void CreateView()
        {
            if (window != null)
            {
                view = new StateSequencer_WorkView();
                blackboard = new StateSequencer_BlackboardView(seq);
                window.GetEditorSkin();
            }
            else
            {
                window = EditorWindow.GetWindow<StateSequencerWindow>();
            }
        }

        void GetEditorSkin()
        {
            guiSkin = (GUISkin)Resources.Load("StateSequencer_GUISkin");
        }
    }
}