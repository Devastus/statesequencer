﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

namespace StateMachine
{
    public class StateSequencer_BlackboardView
    {
        const string instructions = "All Blackboard entries are integers. Transitions can happen with or without any conditions, but the order of evaluation is conditions first, empty second.";

        private Rect viewRect;
        private StateSequencer sequencer;
        private ReorderableList list;

        public StateSequencer_BlackboardView(StateSequencer seq)
        {
            sequencer = seq;
            if(sequencer != null)SetupReorderableList();
        }

        public void UpdateView(Rect editorRect, Rect percentageRect, Event e, StateSequencer seq, GUISkin guiSkin)
        {
            sequencer = seq;
            viewRect = new Rect(editorRect.width * percentageRect.x, editorRect.height * percentageRect.y, editorRect.width * percentageRect.width, editorRect.height * percentageRect.height);

            GUI.Box(viewRect, "Blackboard", guiSkin.GetStyle("BlackboardViewBG"));
            Rect propertyRect = new Rect(viewRect.x + (viewRect.width * 0.04f), 40, viewRect.width * 0.92f, viewRect.height - 50);
            if (list == null && sequencer != null) SetupReorderableList();
            GUILayout.BeginArea(propertyRect);
            if (sequencer != null) list.DoLayoutList();
            if (GUILayout.Button("Instructions"))
            {
                EditorUtility.DisplayDialog("Instructions", instructions, "OK");
            }
            GUILayout.EndArea();

            EditorUtility.SetDirty(seq);
        }

        void SetupReorderableList()
        {
            list = new ReorderableList(sequencer.blackboard, typeof(KeyValue), true, true, true, true);
            list.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
            {
                float width = rect.width * 0.5f;
                KeyValue element = (KeyValue)list.list[index];
                element.key = EditorGUI.TextField(new Rect(rect.x, rect.y + 2, width-2, EditorGUIUtility.singleLineHeight), element.key);
                element._value = EditorGUI.IntField(new Rect(rect.x + width+2, rect.y + 2, width-2, EditorGUIUtility.singleLineHeight), element._value);
            };
            list.drawHeaderCallback = (Rect rect) =>
            {
                EditorGUI.LabelField(rect, new GUIContent("Variables"));
            };
        }
    }
}