﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace StateMachine
{
    [CustomEditor(typeof(StateSequencer))]
    public class StateSequencerEditor : Editor
    {
        StateSequencer seq;
        
        void OnEnable()
        {
            seq = (StateSequencer)target;
        }

        public override void OnInspectorGUI()
        {
            if(GUILayout.Button(new GUIContent("Open State Sequencer"), GUILayout.Height(100)))
            {
                StateSequencerWindow.ShowWindow(seq);
            }
            //seq.active = EditorGUILayout.BeginToggleGroup("Active", seq.active);
            //EditorGUILayout.EndToggleGroup();
        }
    }
}