﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

namespace StateMachine
{
    public class StateSequencer_TransitionWindow : EditorWindow
    {

        private static StateSequencer_TransitionWindow window;
        private StateSequencer sequencer;
        private State state;
        private Transition transition;
        private Vector2 minimumSize = new Vector2(300, 300);

        ReorderableList list;

        public static void ShowWindow(StateSequencer s, State state, Transition transition)
        {
            window = EditorWindow.GetWindow<StateSequencer_TransitionWindow>(true);
            window.sequencer = s;
            window.state = state;
            window.transition = transition;
            window.titleContent = new GUIContent("Transition");
            window.minSize = window.minimumSize;
            window.SetupReorderableList();
        }

        void OnEnable()
        {
            
        }

        void OnGUI()
        {
            if(window == null) EditorWindow.GetWindow<StateSequencer_TransitionWindow>();
            if(sequencer != null && transition != null)
            {
                EditorGUILayout.LabelField(new GUIContent(state.name + " -> " + transition.to.name));
                EditorGUIUtility.labelWidth = 80f;
                transition.type = (Transition.Type)EditorGUILayout.EnumPopup(new GUIContent("Type"), transition.type);
                if(list != null)list.DoLayoutList();

                EditorUtility.SetDirty(sequencer);
                EditorUtility.SetDirty(state);
                //EditorUtility.SetDirty(transition);
            }
        }

        void SetupReorderableList()
        {
            list = new ReorderableList(transition.conditions, typeof(Condition), true, true, true, true);
            list.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
            {
                float third = rect.width / 3;
                Condition element = (Condition)list.list[index];
                if(sequencer.blackboard.Count > 0)
                {
                    element.blackBoardIndex = EditorGUI.Popup(new Rect(rect.x, rect.y+2, third-2, EditorGUIUtility.singleLineHeight), element.blackBoardIndex, GetBlackboardVariableNames());
                    element.type = (Condition.Type)EditorGUI.EnumPopup(new Rect(rect.x + third + 2, rect.y+2, third-2, EditorGUIUtility.singleLineHeight), element.type);
                    element.value = EditorGUI.IntField(new Rect(rect.x + (third * 2) + 2, rect.y+2, third-2, EditorGUIUtility.singleLineHeight), element.value);
                } else
                {
                    EditorGUI.HelpBox(rect, "No Blackboard variables set!", MessageType.Warning);
                }
            };
            list.drawHeaderCallback = (Rect rect) =>
            {
                float width = rect.width * 0.5f;
                EditorGUI.LabelField(new Rect(rect.x, rect.y, width -2, rect.height), new GUIContent("Conditions"));
                transition.evaluation = (Transition.Evaluation)EditorGUI.EnumPopup(new Rect(rect.x + width + 2, rect.y+1, width-2, rect.height), transition.evaluation);
            };
        }

        string[] GetBlackboardVariableNames()
        {
            int length = sequencer.blackboard.Count;
            string[] names = new string[length];
            if (length > 0)
            {
                for (int i = 0; i < length; i++)
                {
                    names[i] = sequencer.blackboard[i].key;
                }
                return names;
            }
            return null;
        }
    }
}