﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;

namespace StateMachine
{
    public class StateSequencer_WorkView
    {
        enum ContextOptions
        {
            Empty,
            Node,
            Transition
        }

        const string MAIN_PATH = "Assets/StateSequencer/";
        const string STATE_TYPE = "t:monoscript";
        static string[] STATES_PATH = { MAIN_PATH + "States" };
        static Color[] HANDLE_COLORS = { new Color(1,1,1), new Color(0.5f,0.75f,1f) };

        private string viewTitle;
        private Rect viewRect;
        private bool connecting = false;
        private State selectedState;
        private State contextState;
        private StateSequencer sequencer;
        private Vector2 mousePos;

        public StateSequencer_WorkView(string title = null)
        {
            viewTitle = title;
        }

        public void UpdateView(Rect editorRect, Rect percentageRect, Event e, StateSequencer seq, GUISkin guiSkin)
        {
            sequencer = seq;

            if(sequencer != null)
            {
                viewTitle = sequencer.name;
            } else
            {
                viewTitle = "Empty";
            }

            viewRect = new Rect(editorRect.width * percentageRect.x, editorRect.height * percentageRect.y, editorRect.width * percentageRect.width, editorRect.height * percentageRect.height);
            GUI.Box(viewRect, viewTitle, guiSkin.GetStyle("WorkViewBG"));

            GUILayout.BeginArea(viewRect);
            if (seq != null)
                UpdateSequencerGUI(viewRect, e, seq, guiSkin);
            GUILayout.EndArea();

            ProcessEvents(e);
        }

        void UpdateSequencerGUI(Rect viewRect, Event e, StateSequencer seq, GUISkin guiSkin)
        {
            int stateCount = seq.states.Count;

            if (connecting)
            {
                DrawConnectionToMouse();
            }

            if (stateCount > 0)
            {
                DrawConnections(e);

                for (int i = 0; i < stateCount; i++)
                {
                    if (Application.isPlaying)
                    {
                        if (seq.CurrentState == seq.states[i])
                        {
                            seq.states[i].Selected = true;
                        }
                        else
                        {
                            seq.states[i].Selected = false;
                        }
                    }
                    seq.states[i].UpdateStateGUI(viewRect, e, guiSkin);
                }

                
            }

            EditorUtility.SetDirty(seq);
        }

        void ProcessEvents(Event e)
        {
            mousePos = e.mousePosition;
            if (viewRect.Contains(mousePos))
            {
                //If Left-Click
                if(e.button == 0)
                {
                    //Select / Deselect a node
                    if(e.type == EventType.MouseDown)
                    {
                        GUIUtility.hotControl = 0;
                        GUIUtility.keyboardControl = 0;
                        DeselectAll();
                        selectedState = GetNode(true, e);
                        if (selectedState == null)
                        {
                            DeselectAll();
                        } else
                        {
                            if (connecting)
                            {
                                ConnectStates();
                            } else
                            {
                                Selection.activeObject = selectedState;
                            }
                        }
                        if (connecting) connecting = false;
                    }

                    //Drag the selected node
                    if (e.type == EventType.MouseDrag)
                    {
                        if (selectedState != null && selectedState.Selected) selectedState.Drag(e);
                    }
                }

                //If Right-Click
                if(e.button == 1)
                {
                    if(e.type == EventType.MouseDown)
                    {
                        //Show context menu
                        connecting = false;
                        contextState = GetNode(false, e);
                        bool inNode = contextState != null;
                        ProcessContextMenu(e, inNode ? ContextOptions.Node : ContextOptions.Empty);
                    }
                }

                //If Middle-Click
                if(e.button == 2)
                {
                    //Move the view, ie. drag all nodes
                    if(e.type == EventType.MouseDrag)
                    {
                        int length = sequencer.states.Count;
                        for (int i = 0; i < length; i++)
                        {
                            sequencer.states[i].Drag(e);
                        }
                    }
                }

                if(e.keyCode == KeyCode.Delete)
                {
                    if(selectedState != null)
                    {
                        RemoveNode(selectedState);
                        selectedState = null;
                    }
                }
            }
        }

        void ProcessContextMenu(Event e, ContextOptions options)
        {
            var menu = new GenericMenu();

            switch (options)
            {
                case ContextOptions.Empty:
                    string[] directories = AssetDatabase.GetSubFolders(MAIN_PATH + "States");
                    for (int i = 0; i < directories.Length; i++)
                    {
                        string[] guids = AssetDatabase.FindAssets(STATE_TYPE, new string[] { directories[i] });
                        foreach (var guid in guids)
                        {
                            string path = AssetDatabase.GUIDToAssetPath(guid);
                            menu.AddItem(new GUIContent(Path.GetFileName(directories[i]) + "/" + Path.GetFileNameWithoutExtension(path)), false, AddNode, AssetDatabase.LoadAssetAtPath<MonoScript>(path));
                        }
                    }
                    menu.AddSeparator("");
                    menu.AddItem(new GUIContent("Link"), false, AddNode, AssetDatabase.LoadAssetAtPath<MonoScript>(MAIN_PATH + "Link.cs"));
                    menu.AddItem(new GUIContent("Exit"), false, AddNode, AssetDatabase.LoadAssetAtPath<MonoScript>(MAIN_PATH + "Exit.cs"));
                    break;
                case ContextOptions.Node:
                    menu.AddItem(new GUIContent("Make Transition"), false, BeginConnect);
                    menu.AddSeparator("");
                    menu.AddItem(new GUIContent("Set Root"), false, SetRootNode);
                    menu.AddItem(new GUIContent("Remove State"), false, RemoveNode, contextState);
                    break;
                case ContextOptions.Transition:
                    
                    break;
            }

            menu.ShowAsContext();
            e.Use();
        }

        #region Node Utility Methods

        State GetNode(bool select, Event e)
        {
            State selected = null;
            int length = sequencer.states.Count;
            for (int i = 0; i < length; i++)
            {
                selected = sequencer.states[i].SelectNode(select, e.mousePosition);
                if (selected != null) break;
            }

            return selected;
        }

        void DeselectAll()
        {
            int length = sequencer.states.Count;
            for (int i = 0; i < length; i++)
            {
                sequencer.states[i].Selected = false;
            }
            selectedState = null;
            Selection.activeObject = null;
        }

        void AddNode(object target)
        {
            if(target != null && sequencer != null)
            {
                MonoScript script = (MonoScript)target;
                System.Type type = script.GetClass();
                var obj = ScriptableObject.CreateInstance(type);
                State state = (State)obj;
                state.name = type.ToString();
                state.rect.position = mousePos;
                sequencer.states.Add(state);
                if(sequencer.states.Count == 1)
                {
                    state.isRoot = true;
                    sequencer.rootIndex = 0;
                }
                AssetDatabase.AddObjectToAsset(state, sequencer);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
                contextState = null;
            }
        }

        void RemoveNode(object target)
        {
            if(sequencer != null)
            {
                State state = (State)target;
                if (selectedState == state) selectedState = null;
                sequencer.states.Remove(state);
                GameObject.DestroyImmediate(state, true);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
                contextState = null;
                CheckAllTransitions();
            }
        }

        void SetRootNode()
        {
            if(sequencer != null)
            {
                int length = sequencer.states.Count;
                for (int i = 0; i < length; i++)
                {
                    sequencer.states[i].isRoot = false;
                }
                contextState.isRoot = true;
                sequencer.rootIndex = sequencer.states.IndexOf(contextState);
                contextState = null;
            }
        }

        /// <summary>
        /// Begin the connection process
        /// </summary>
        void BeginConnect()
        {
            if(sequencer != null)
            {
                connecting = true;
            }
        }

        /// <summary>
        /// Check that we don't try to connect to ourselves, or something we've already connected into, then perform connection
        /// </summary>
        void ConnectStates()
        {
            int length = contextState.transitions.Count;
            bool hasTransition = false;
            for (int i = 0; i < length; i++)
            {
                if (contextState.transitions[i].to == selectedState)
                {
                    hasTransition = true;
                    break;
                }
            }
            if (selectedState != contextState && !hasTransition) contextState.CreateTransition(selectedState);
            contextState = null;
        }

        void RemoveTransition(object target)
        {
            if(target != null)
            {
                Transition transition = (Transition)target;
                transition.Remove();
            }
        }

        /// <summary>
        /// Check through all Transitions and clean up those that are null
        /// </summary>
        void CheckAllTransitions()
        {
            int stateLength = sequencer.states.Count;
            for (int i = 0; i < stateLength; i++)
            {
                int tLength = sequencer.states[i].transitions.Count;
                for (int a = 0; a < tLength; a++)
                {
                    if (sequencer.states[i].transitions[a].to == null) sequencer.states[i].transitions.Remove(sequencer.states[i].transitions[a]);
                }
            }
        }

        void DrawConnectionToMouse()
        {
            if (connecting)
            {
                Handles.BeginGUI();
                Handles.color = HANDLE_COLORS[1];
                Vector3 output = new Vector3(contextState.rect.x + contextState.rect.width * 0.5f, contextState.rect.y + contextState.rect.height * 0.5f);
                Handles.DrawLine(output, mousePos);
                Handles.EndGUI();
            }
        }

        void DrawConnections(Event e)
        {
            Handles.BeginGUI();
            Handles.color = HANDLE_COLORS[0];
            int length = sequencer.states.Count;
            if(length > 1)
            {
                for (int i = 0; i < length; i++)
                {
                    int tLength = sequencer.states[i].transitions.Count;
                    if (tLength < 1) continue;
                    for (int a = 0; a < tLength; a++)
                    {
                        if(sequencer.states[i].transitions[a] != null)
                        {
                            State toState = sequencer.states[i].transitions[a].to;
                            Vector2 dir = (toState.rect.position - sequencer.states[i].rect.position).normalized;
                            Vector2 right = (Vector2)Vector3.Cross(dir, new Vector3(0, 0, -1)) * 5f;
                            Vector2 outputOffset = sequencer.states[i].inputCount > 0 ? new Vector2(sequencer.states[i].rect.width * 0.5f + right.x, sequencer.states[i].rect.height * 0.5f + right.y) : new Vector2(sequencer.states[i].rect.width * 0.5f, sequencer.states[i].rect.height * 0.5f);
                            Vector2 inputOffset = toState.inputCount > 0 ? new Vector2(toState.rect.width * 0.5f + right.x, toState.rect.height * 0.5f + right.y) : new Vector2(toState.rect.width * 0.5f, toState.rect.height * 0.5f);
                            Vector3 output = new Vector3(sequencer.states[i].rect.x + outputOffset.x, sequencer.states[i].rect.y + outputOffset.y);
                            Vector3 input = new Vector3(toState.rect.x + inputOffset.x, toState.rect.y + inputOffset.y);
                            Vector3 midPoint = output + (input - output) * 0.5f;
                            midPoint.z = -10;
                            Vector3 arrowPoint = output + (input - output) * 0.7f;
                            arrowPoint.z = -10;
                            Rect buttonRect = new Rect(midPoint.x-5, midPoint.y-5, 10, 10);
                            Color color;
                            if (buttonRect.Contains(mousePos))
                            {
                                color = HANDLE_COLORS[1];
                                if (e.type == EventType.MouseDown)
                                {
                                    if (e.button == 0)
                                    {
                                        StateSequencer_TransitionWindow.ShowWindow(sequencer, sequencer.states[i], sequencer.states[i].transitions[a]);
                                        e.Use();
                                    }
                                    else if (e.button == 1)
                                    {
                                        var menu = new GenericMenu();
                                        menu.AddItem(new GUIContent("Remove Transition"), false, RemoveTransition, sequencer.states[i].transitions[a]);
                                        menu.ShowAsContext();
                                        e.Use();
                                    }
                                }
                            }
                            else
                            {
                                color = HANDLE_COLORS[0];
                            }
                            Handles.color = color;
                            Handles.DrawBezier(output, input, output, input, color, null, 3);
                            //Handles.SphereCap(1000, midPoint, Quaternion.identity, 6);
                            Handles.ConeCap(1000, midPoint, Quaternion.LookRotation((input - output).normalized, Vector3.up), 9);
                            //Quaternion.LookRotation((input-output).normalized, Vector3.up)
                        }
                    }
                }
            }
            Handles.EndGUI();
        }
        #endregion
    }
}
