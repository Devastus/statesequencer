﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateMachine;

public class State_Idle : State {

    public float randomRange = 5f;
    public float speed = 4f;
    private Vector3 randomPos;

    Collider[] colliders;
    AIController control;

    public override IEnumerator Init(StateSequencer seq)
    {
        randomPos = seq.runner.position + new Vector3(  Random.Range(-randomRange, randomRange),
                                                        0,
                                                        Random.Range(-randomRange, randomRange));
        if (control == null) seq.runner.GetComponent<AIController>();
        return base.Init(seq);
    }

    public override IEnumerator Run(StateSequencer seq)
    {
        float timer = 0f;
        float t = 0f;
        Vector3 oldPos = seq.runner.position;
        float closePoint = 0.001f;
        int count;
        while (true)
        {
            colliders = Physics.OverlapSphere(seq.runner.position, 5f, LayerMask.GetMask("Coins"), QueryTriggerInteraction.Collide);
            seq.SetValue("CoinCount", colliders.Length);

            float distance = (seq.runner.position - randomPos).sqrMagnitude;
            if (distance < closePoint) break;

            timer += Time.deltaTime;
            t = Mathf.Clamp01(timer / 3f);
            t = t * t * (3f - 2f * t);
            seq.runner.position = Vector3.Lerp(oldPos, randomPos, t);
            yield return null;
        }
    }

    public override IEnumerator End(StateSequencer seq)
    {
        seq.RestartState();
        return base.Init(seq);
    }

}
