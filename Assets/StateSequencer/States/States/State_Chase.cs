﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateMachine;

public class State_Chase : State {

    public Transform target;

    Collider[] colliders;
    int count;

    public override IEnumerator Init(StateSequencer seq)
    {
        colliders = Physics.OverlapSphere(seq.runner.position, 5f, LayerMask.GetMask("Coins"), QueryTriggerInteraction.Collide);
        if (colliders.Length > 0) target = colliders[0].transform;
        return base.Init(seq);
    }

    public override IEnumerator Run(StateSequencer seq)
    {
        if(target != null)
        {
            Vector3 targetPos = new Vector3(target.position.x, seq.runner.position.y, target.position.z);
            yield return Tween.MovePosition(seq.runner, targetPos, 3f, Tween.Type.Smoothstep);
        } else
        {
            yield break;
        }
    }

    public override IEnumerator End(StateSequencer seq)
    {
        seq.SetValue("CoinCount", 0);
        return base.End(seq);
    }

}
