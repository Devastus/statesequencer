﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace StateMachine
{
    public class Link : State
    {
        /* Link acts as the Sub-State Machine of State Sequencer with the main difference that the Sub-Sequencers are independent of the Parent Sequencer.
         * However, Parent Sequencer waits for the Sub-Sequencer to finish before advancing any further.
         */

        public StateSequencer subSequencer;

        public override IEnumerator Init(StateSequencer seq)
        {
            yield break;
        }

        public override IEnumerator Run(StateSequencer seq)
        {
            StateSequencerManager.StartSequencer(subSequencer);
            while (subSequencer.active)
            {
                yield return null;
            }
        }

        public override IEnumerator End(StateSequencer seq)
        {
            yield break;
        }

#if UNITY_EDITOR
        public override GUIStyle ChooseSkin(GUISkin guiSkin)
        {
            if (isRoot)
            {
                return Selected ? guiSkin.GetStyle("RootNodeBG_Selected") : guiSkin.GetStyle("RootNodeBG");
            }
            else
            {
                return Selected ? guiSkin.GetStyle("LinkBG_Selected") : guiSkin.GetStyle("LinkBG");
            }
        }
#endif

    }
}