﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace StateMachine
{
    [System.Serializable]
    public abstract class State : ScriptableObject
    {
        public enum Cycle
        {
            Init,
            Run,
            End
        }

        [SerializeField]
        [HideInInspector]
        public List<Transition> transitions = new List<Transition>();
        [SerializeField]
        [HideInInspector]
        public bool isRoot = false;
        [HideInInspector]
        public Cycle cycle;

        /// <summary>
        /// Initialize a state.
        /// </summary>
        public virtual IEnumerator Init(StateSequencer seq)
        {
            yield break;
        }

        /// <summary>
        /// Run the State logic.
        /// </summary>
        public virtual IEnumerator Run(StateSequencer seq)
        {
            yield break;
        }

        /// <summary>
        /// Perform State end stuff like unloading, preparing etc.
        /// </summary>
        public virtual IEnumerator End(StateSequencer seq)
        {
            yield break;
        }

        public IEnumerator GetRoutine(StateSequencer seq)
        {
            switch (cycle)
            {
                case Cycle.Init: return Init(seq);
                case Cycle.Run: return Run(seq);
                case Cycle.End: return End(seq);
                default: return null;
            }
        }

        /// <summary>
        /// Check whether any Transition's condition is met, react according to Evaluation requirements
        /// </summary>
        public bool CheckForConditions(StateSequencer seq)
        {
            int tLength = transitions.Count;
            bool pass = false;
            int index = 0;
            if(tLength > 0)
            {
                for (int i = 0; i < tLength; i++)
                {
                    if (transitions[i] != null)
                    {
                        int cLength = transitions[i].conditions.Count;
                        if (cLength > 0)
                        {
                            for (int c = 0; c < cLength; c++)
                            {
                                if (transitions[i].evaluation == Transition.Evaluation.One)
                                {
                                    if (transitions[i].conditions[c].Evaluate(seq) == true)
                                    {
                                        seq.OnTransition(transitions[i]);
                                        return true;
                                    }
                                }
                                else
                                {
                                    pass = transitions[i].conditions[c].Evaluate(seq) == true;
                                    index = i;
                                    if (pass == false) return false; 
                                }
                            }
                        }
                    }
                }
            }

            if(pass) seq.OnTransition(transitions[index]);
            return pass;
        }

#if UNITY_EDITOR
        public string label;
        [HideInInspector]
        public Rect rect = new Rect(0,0,130,24);
        [HideInInspector]
        public int inputCount = 0;
        public bool Selected { get; set; }

        public void UpdateStateGUI(Rect viewRect, Event e, GUISkin guiSkin)
        {
            GUI.Box(rect, "", ChooseSkin(guiSkin));
            EditorGUI.LabelField(rect, label != null && label != "" ? label : name, Selected ? guiSkin.GetStyle("NodeLabel_Selected") : guiSkin.GetStyle("NodeLabel"));
            EditorUtility.SetDirty(this);
        }

        /// <summary>
        /// Connect this State to another via a Transition
        /// </summary>
        /// <param name="toStateIndex">State to transition to</param>
        public void CreateTransition(State toState)
        {
            Transition transition = new Transition(this, toState);
            toState.inputCount += 1;
            transitions.Add(transition);
        }

        /// <summary>
        /// Check whether this State/Node has been selected or picked
        /// </summary>
        /// <param name="select"></param>
        /// <param name="mousePos"></param>
        /// <returns></returns>
        public State SelectNode(bool select, Vector2 mousePos)
        {
            if (rect.Contains(mousePos))
            {
                if (select)
                {
                    Selected = true;
                } else
                {
                    Selected = false;
                }
                return this;
            }
            Selected = false;
            return null;
        }

        /// <summary>
        /// Drag this State/Node
        /// </summary>
        /// <param name="e"></param>
        public void Drag(Event e)
        {
            rect.x += e.delta.x;
            rect.y += e.delta.y;
        }

        /// <summary>
        /// Get a position relative to the viewRect position
        /// </summary>
        /// <param name="position">The child rect of viewRect to compare against</param>
        /// <returns>Rect</returns>
        public Rect GetPositionRelative(Rect position)
        {
            return new Rect(rect.x + position.x, rect.y + position.y, position.width, position.height);
        }

        public Vector2 GetPositionRelative(Vector2 position)
        {
            return new Vector2(rect.x + position.x, rect.y + position.y);
        }

        public virtual GUIStyle ChooseSkin(GUISkin guiSkin)
        {
            if (isRoot)
            {
                return Selected ? guiSkin.GetStyle("RootNodeBG_Selected") : guiSkin.GetStyle("RootNodeBG");
            }
            else
            {
                return Selected ? guiSkin.GetStyle("NodeBG_Selected") : guiSkin.GetStyle("NodeBG");
            }
        }
        #endif
    }

    //The data pack for performing Transitions, it should contain the State to be transitioned into and the conditions for this transition to happen
    //The variable that is checked against the condition should probably be an global _object_ type that can be accessed from the state machine itself, if we want flexible editing
    //Editor script would handle "typing" the input (showing you the desired EditorField, which will in turn typecast the object into a given type (int, float, bool))
    //The type "object" might introduce optimization problems in itself though.
    //Alternatively, we can decide that the State Machine works on a single value type (like int) and work with that. Less flexibility, less overhead.
    //Nevertheless, we want the usage of these variables' names happen solely inside the editor via Dropdown menu choosing, and the ID:ing at runtime happening by numbers
    [System.Serializable]
    public class Transition
    {
        public enum Type
        {
            Safe,
            Overwrite
        }

        public enum Evaluation
        {
            One,
            All
        }

        [SerializeField]
        public State from;
        [SerializeField]
        public State to;
        [SerializeField]
        public Type type;
        [SerializeField]
        public Evaluation evaluation;
        [SerializeField]
        public List<Condition> conditions = new List<Condition>();

        public Transition(State from, State to)
        {
            this.from = from;
            this.to = to;
            type = Type.Safe;
            evaluation = Evaluation.One;
        }

        public void Remove()
        {
            to.inputCount -= 1;
            from.transitions.Remove(this);
        }
    }

    [System.Serializable]
    public class Condition
    {
        public enum Type
        {
            Equal,
            Greater,
            Lesser
        }

        [SerializeField]
        public Type type;
        [SerializeField]
        public int blackBoardIndex;
        [SerializeField]
        public int value;

        public bool Evaluate(StateSequencer seq)
        {
            if(seq.blackboard[blackBoardIndex] != null)
            {
                switch (type)
                {
                    case Type.Equal:
                        return seq.blackboard[blackBoardIndex]._value == value;
                    case Type.Greater:
                        return seq.blackboard[blackBoardIndex]._value > value;
                    case Type.Lesser:
                        return seq.blackboard[blackBoardIndex]._value < value;
                }
            }
            return false;
        }
    }
}