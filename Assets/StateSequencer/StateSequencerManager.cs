﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateMachine;

public class StateSequencerManager : MonoBehaviour {

    private static StateSequencerManager _instance;
    public static StateSequencerManager Instance { get {
            if (_instance == null) _instance = FindObjectOfType<StateSequencerManager>();
            if (_instance == null) _instance = new GameObject("State Sequencer Manager").AddComponent<StateSequencerManager>();
            return _instance;
        } }

    public static void StartSequencer(StateSequencer seq, Transform runner = null)
    {
        seq.coroutine = StateSequencerManager.Instance.StartCoroutine(seq.Run(runner));
    }

    public static void StopSequencer(StateSequencer seq)
    {
        StateSequencerManager.Instance.StopCoroutine(seq.coroutine);
    }

    public static Coroutine StartRoutine(IEnumerator routine)
    {
        return StateSequencerManager.Instance.StartCoroutine(routine);
    }
}
