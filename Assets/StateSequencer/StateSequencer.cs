﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StateMachine
{
    [System.Serializable]
    [CreateAssetMenu(menuName = "State Sequencer")]
    public class StateSequencer : ScriptableObject
    {
        public System.Action<Transition> OnTransition;      //For performing a Transition from State to State

        [SerializeField]
        public List<State> states = new List<State>();  //Our library of States inside this State Sequencer
        [SerializeField]
        public List<KeyValue> blackboard = new List<KeyValue>(); //Our library of values we can use inside the StateMachine (currently only integer values)
        [SerializeField]
        public int rootIndex = 0;                       //From which State do we begin the State Machine

        [HideInInspector]
        public bool active = false;                     //Is the State Machine active
        [HideInInspector]
        public Coroutine coroutine;                     //Stored for possible stopping mechanisms
        State currentState;                             //Our current State
        public State CurrentState { get { return currentState; } }
        IEnumerator activeRoutine;                      //Our current routine to process
        Transition transition = null;                   //Stored transition from the transitioning State
        bool inTransition = false;                      //Are we in transition

        [HideInInspector]
        public Transform runner;

        void OnEnable()
        {
            OnTransition = TransitionTo;
        }

        void OnDisable()
        {
            OnTransition = null;
        }

        /// <summary>
        /// Start & Run the State Sequencer main loop
        /// </summary>
        /// <returns></returns>
        public IEnumerator Run(Transform runner)
        {
            if (states.Count == 0) yield break;
            this.runner = runner;
            inTransition = false;
            currentState = states[rootIndex];
            currentState.cycle = State.Cycle.Init;
            activeRoutine = currentState.Init(this);
            active = true;

            while (active == true)
            {
                //Check first whether we meet any conditions for a Transition to occur
                if (currentState.cycle == State.Cycle.Run) currentState.CheckForConditions(this);

                if (!inTransition)  //If we're not trying a Transition
                {
                    if (activeRoutine.MoveNext())
                    {
                        yield return activeRoutine.Current;
                    }
                    else //If we're at the end of a routine, let's see if we can advance to the next Cycle
                    {
                        if (currentState.cycle != State.Cycle.End)
                        {
                            currentState.cycle = (State.Cycle)((int)currentState.cycle + 1);
                            activeRoutine = currentState.GetRoutine(this);
                        }
                        else //If we're at the end of a State and inTransition = false, we'll re-check for conditions. 
                        //If false, try a conditionless Transition or stop the State Machine altogether.       
                        {
                            if (currentState.CheckForConditions(this) == false)
                            {
                                if (!TryTransition())
                                {
                                    currentState = null;
                                    break;
                                }
                            }
                        }
                    }
                }
                else //We are currently in a Transition, depending on the type we either let the End() routine finish or overwrite with the new State's Init()
                {
                    if (activeRoutine.MoveNext() && transition.type == Transition.Type.Safe)
                    {
                        yield return activeRoutine.Current;
                    }
                    else
                    {
                        currentState = transition.to;
                        transition = null;
                        inTransition = false;
                        if (currentState != null)
                        {
                            currentState.cycle = State.Cycle.Init;
                            activeRoutine = currentState.Init(this);
                        }
                        else
                        {
                            Debug.LogError("State Sequencer failed to Transition into a new state -- currentState is null!");
                            break;
                        }
                    }
                }
            }
            active = false;
        }

        public void RestartState()
        {
            currentState.cycle = State.Cycle.Init;
            activeRoutine = currentState.Init(this);
        }

        /// <summary>
        /// Start a Transition
        /// </summary>
        public void TransitionTo(Transition t)
        {
            if(t != null)
            {
                inTransition = true;
                currentState.cycle = State.Cycle.End;
                activeRoutine = currentState.End(this);
                transition = t;
            }
        }

        /// <summary>
        /// Try a conditionless Transition
        /// </summary>
        /// <returns>Whether succesful or not</returns>
        public bool TryTransition()
        {
            int tLength = currentState.transitions.Count;
            for (int i = 0; i < tLength; i++)
            {
                int cLength = currentState.transitions[i].conditions.Count;
                if(cLength == 0)
                {
                    currentState = currentState.transitions[i].to;
                    currentState.cycle = State.Cycle.Init;
                    activeRoutine = currentState.Init(this);
                    return true;
                }
            }
            return false;
        }

        #region Blackboard Utility Methods

        public int GetValue(string key)
        {
            int length = blackboard.Count;
            int value = 0;
            if (length > 0)
            {
                for (int i = 0; i < length; i++)
                {
                    if (blackboard[i].key == key)
                    {
                        value = blackboard[i]._value;
                        break;
                    }
                }
            }
            return value;
        }

        public int GetValue(int index)
        {
            return blackboard[index]._value;
        }

        public void SetValue(string key, int value)
        {
            int length = blackboard.Count;
            if (length > 0)
            {
                for (int i = 0; i < length; i++)
                {
                    if (blackboard[i].key == key)
                    {
                        blackboard[i]._value = value;
                        return;
                    }
                }
            }
        }

        public void SetValue(int index, int value)
        {
            blackboard[index]._value = value;
        }

        public int GetIndexOf(string key)
        {
            int length = blackboard.Count;
            int value = 0;
            if (length > 0)
            {
                for (int i = 0; i < length; i++)
                {
                    if (blackboard[i].key == key)
                    {
                        value = i;
                        break;
                    }
                }
            }
            return value;
        }

        #endregion
    }
}
