﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace StateMachine
{
    public class Exit : State
    {
        /* Exit is a dummy state that acts as the "exit out of this State Machine" coefficient.
         * 
         * FIXME: You can add transitions to it, add labels to it, practically it acts as any other state, which is not how it should be.
         * This state should only accept transitions _into_ it and always end the State Machine process.
         */
        public override IEnumerator Init(StateSequencer seq)
        {
            yield break;
        }

        public override IEnumerator Run(StateSequencer seq)
        {
            yield break;
        }

        public override IEnumerator End(StateSequencer seq)
        {
            yield break;
        }

#if UNITY_EDITOR
        public override GUIStyle ChooseSkin(GUISkin guiSkin)
        {
            if (isRoot)
            {
                return Selected ? guiSkin.GetStyle("RootNodeBG_Selected") : guiSkin.GetStyle("RootNodeBG");
            }
            else
            {
                return Selected ? guiSkin.GetStyle("ExitBG_Selected") : guiSkin.GetStyle("ExitBG");
            }
        }
#endif
    }
}