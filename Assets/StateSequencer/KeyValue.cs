﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class KeyValue {
    //public enum Type
    //{
    //    Float,
    //    Int,
    //    Bool
    //}

    public string key;
    //public Type type;
    public int _value;
    //public float Value
    //{
    //    get
    //    {
    //        switch (type)
    //        {
    //            default: return _value;
    //            case Type.Int: return (int)_value;
    //            case Type.Bool: return _value >= 0.5f ? 1 : 0;
    //        }
    //    }
    //    set
    //    {
    //        switch (type)
    //        {
    //            default: _value = value; break;
    //            case Type.Int: _value = (int)value; break;
    //            case Type.Bool: _value = value >= 0.5f ? 1 : 0; break;
    //        }
    //    }
    //}

    public KeyValue()
    {
        key = default(string);
        _value = default(int);
    }

    public KeyValue(string key, int value)
    {
        this.key = key;
        this._value = value;
    }
}
