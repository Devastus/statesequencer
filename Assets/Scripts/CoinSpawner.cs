﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSpawner : MonoBehaviour {

    public float randomRange = 10f;
    public float interval = 1f;
    public GameObject coinPrefab;
    public bool active = true;

    float timer;
	
	// Update is called once per frame
	void Update () {
        if (active)
        {
            if (Time.time > timer)
            {
                timer = Time.time + interval;
                Vector3 spawnPos = new Vector3(Random.Range(-randomRange, randomRange),
                                                4f,
                                                Random.Range(-randomRange, randomRange));

                Instantiate(coinPrefab, spawnPos, Quaternion.identity);
            }
        }
	}
}
