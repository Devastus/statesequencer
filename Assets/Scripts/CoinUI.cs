﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinUI : MonoBehaviour {

    public static System.Action OnCoinPickup;

    public Text coinText;
    public int coins = 0;

    void OnEnable()
    {
        OnCoinPickup = IncreaseCoins;
    }

    void IncreaseCoins()
    {
        coins++;
        coinText.text = "Coins: " + coins;
    }
}
