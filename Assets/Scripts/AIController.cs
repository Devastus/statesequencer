﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateMachine;

public class AIController : MonoBehaviour {

    public StateSequencer sequencer;
    public float radius = 5f;
    public LayerMask layerMask;

    Collider[] colliders;
    float timer;
    float cooldownTime = 1f;

	// Use this for initialization
	void Start () {
        StateSequencerManager.StartSequencer(sequencer, transform);
	}
	
}
