﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleCam : MonoBehaviour {

    public Transform target;
    public float speed;
    public float distance = 5f;
	
	// Update is called once per frame
	void LateUpdate () {
        Vector3 tPos = target.position - transform.forward * distance;
        transform.position = Vector3.Lerp(transform.position, tPos, Time.deltaTime * speed);
	}
}
